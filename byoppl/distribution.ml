type 'a support = {
  values : 'a array;
  logits : float array;
  probs : float array;
}

type 'a t = {
  sample : unit -> 'a;
  logpdf : 'a -> float;
  mean : (unit -> float) option;
  var : (unit -> float) option;
  samples : 'a array Lazy.t;
  support : 'a support option;
}

exception EmptySupport

let make ?(n = 10000) ~sample ~logpdf ?mean ?var ?support () =
  let samples = lazy (Array.init n (fun _ -> sample ())) in
  { sample; logpdf; mean; var; samples; support }

let draw dist = dist.sample ()
let get_samples dist = Lazy.force dist.samples

let get_support ?(shrink = false) dist =
  match shrink with
  | false -> Option.get dist.support
  | true ->
      let { values; probs; _ } = Option.get dist.support in
      let values, probs = Utils.shrink ~values ~probs in
      { values; logits = Array.map log probs; probs }

let logpdf dist x = dist.logpdf x

let mean_generic ~transform dist : float =
  match (dist.mean, dist.support) with
  | Some mean, _ -> mean ()
  | _, Some { values; logits; _ } ->
      let values = values |> Array.map transform |> Utils.to_owl_arr in
      let logits = Utils.to_owl_arr logits in
      Utils.average ~values ~logits
  | _ -> Owl_stats.mean (dist |> get_samples |> Array.map transform)

let var_generic ~transform dist =
  match (dist.var, dist.support) with
  | Some var, _ -> var ()
  | _, Some { values; logits; _ } ->
      let m = mean_generic ~transform dist in
      let values =
        values |> Array.map transform |> Utils.to_owl_arr
        |> Owl.Arr.(fun a -> (a -$ m) **$ 2.)
      in
      let logits = Utils.to_owl_arr logits in
      Utils.average ~values ~logits
  | _ -> Owl_stats.var (dist |> get_samples |> Array.map transform)

let mean = mean_generic ~transform:Fun.id
let mean_int = mean_generic ~transform:Float.of_int
let var = var_generic ~transform:Fun.id
let var_int = var_generic ~transform:Float.of_int
let std dist = sqrt (var dist)
let std_int dist = sqrt (var_int dist)
let stats dist = (mean dist, std dist)
let stats_int dist = (mean_int dist, std_int dist)

let support ~values ~logits =
  assert (Array.length values = Array.length logits);
  begin try
    assert (Array.length values > 0) ;
    assert (Array.exists ((<>) (-.infinity)) logits)
  with | Assert_failure _ -> raise EmptySupport end ;
  let probs = Utils.normalize logits in
  let support = { values; logits; probs } in
  let sample () =
    let i = Owl_stats.categorical_rvs probs in
    values.(i)
  in
  let logpdf x = match Utils.findi values x with
      | None -> -.infinity
      | Some i -> logits.(i)
  in
  make ~sample ~logpdf ~support ()

let uniform_support ~values =
  support ~values ~logits:(Array.make (Array.length values) 0.)

let map ?(shrink = false) ?finv f dist =
  let { values; logits; _ } = get_support dist in
  let values = values |> Array.map f in
  let dist_support = support ~values ~logits in
  let sample () = f (dist.sample ()) in
  let logpdf x = match finv with
    | None -> dist_support.logpdf x
    | Some f -> dist.logpdf (f x) in
  let support = get_support ~shrink dist_support in
  make ~sample ~logpdf ~support ()

let split dist = map fst dist, map snd dist

let split_list dist =
  let { values; logits; _ } = get_support dist in
  assert (Array.length values > 0);
  assert (Array.for_all (fun v -> List.length v = List.length values.(0)) values);
  let rec split res sup =
    if Array.for_all (( = ) []) sup then res
    else
      let res = split res (Array.map (fun x -> List.tl x) sup) in
      let values = Array.map (fun x -> List.hd x) sup in
      support ~values ~logits :: res
  in
  split [] values

let split_array dist =
  let { values; logits; _ } = get_support dist in
  let values = Array.map Array.to_list values in
  let d = split_list (support ~values ~logits) in
  Array.of_list d

(* 1. Discrete probability distributions *)

(* https://en.wikipedia.org/wiki/Bernoulli_distribution *)
let bernoulli ~p =
  assert (0. <= p && p <= 1.);
  let sample () = Owl_stats.binomial_rvs ~p ~n:1 in
  let logpdf x = Owl_stats.binomial_logpdf x ~p ~n:1 in
  let mean () = p in
  let var () = p *. (1. -. p) in
  let support =
    {
      values = [| 0; 1 |];
      logits = [| log (1. -. p); log p |];
      probs = [| 1. -. p; p |];
    }
  in
  make ~sample ~logpdf ~support ~mean ~var ()

(* https://en.wikipedia.org/wiki/Beta-binomial_distribution *)
let betabinomial ~n ~a ~b =
  let n_float = Float.of_int n in
  assert (n >= 0 && a > 0. && b > 0.);
  let sample () = Owl.Dense.Ndarray.Any.(
      init [| n |] Owl_stats.(fun _ -> let p = beta_rvs ~a ~b in
                               binomial_rvs ~p ~n:1)
      |> fold (+) 0
    ) in
  let logpdf x = Owl.Maths.(combination_float n x *. beta Float.(of_int x +. a) Float.(n_float -. of_int x +. b)
                            /. beta a b) in
  let mean () = n_float *. a /. (a +. b) in
  let var () = n_float *. a *. b *. (n_float +. a +. b) /. ((a +. b) ** 2. *. (a +. b +. 1.)) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Binomial_distribution *)
let binomial ~p ~n =
  assert (n >= 0 && 0. <= p && p <= 1.);
  let sample () = Owl_stats.binomial_rvs ~p ~n in
  let logpdf x = Owl_stats.binomial_logpdf x ~p ~n in
  let mean () = Float.of_int n *. p in
  let var () = Float.of_int n *. p *. (1. -. p) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Dirac_distribution *)
let dirac ~v =
  let sample () : 'a = v in
  let logpdf x = if x = v then 0. else -.infinity in
  make ~sample ~logpdf ()

(* https://en.wikipedia.org/wiki/Geometric_distribution *)
(* let geometric = negbinomial ~r:1  *)
let geometric ~p =
  assert (0. <= p && p <= 1.);
  let sample () =
    let x = Owl_stats.uniform_rvs ~a:0. ~b:1. in
    Float.(log (1. -. x) /. log (1. -. p) |> ceil |> to_int) in
  let logpdf x = (Float.of_int x -. 1.) *. log (1. -. p) +. log p in
  let mean () = 1. /. p in
  let var () = (1. -. p) /. p ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Hypergeometric_distribution *)
let hypergeometric ~good ~bad ~sample =
  let good_float, bad_float, sample_float = Float.(of_int good, of_int bad, of_int sample) in
  assert (good >= 0 && good <= sample && bad >= 0 && bad <= sample);
  let sample_fun () = Owl_stats.hypergeometric_rvs ~good ~bad ~sample in
  let logpdf x = Owl_stats.hypergeometric_logpdf x ~good ~bad ~sample in
  let mean () = good_float *. bad_float /. sample_float in
  let var () = good_float *. bad_float *. (sample_float -. good_float) *. (sample_float -. bad_float) /. (sample_float ** 2. *. (sample_float -. 1.)) in
  make ~sample:sample_fun ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Negative_binomial_distribution *)
let negbinomial ~r ~p =
  let r_float = Float.of_int r in
  assert (r > 0 && 0. <= p && p <= 1.);
  let sample () =
    (* let x = Owl_stats.uniform_rvs ~a:0. ~b:1. in *)
    (* let cdf k = *)
    (*   let a = Float.of_int k +. 1. in *)
    (*   let b = r_float in *)
    (*   1. -. Owl.Maths.(betainc a b p /. beta a b) in *)
    (* let k = ref 0 in *)
    (* while x < cdf (!k + 1) do incr k done ; *)
    (* !k in *)
    let successes = ref 0 in
    let failures = ref 0 in
    while !failures < r do
      if Owl_stats.binomial_rvs ~p ~n:1 = 1 then
        incr successes
      else
        incr failures
    done ;
    !successes in
  let logpdf x = log Float.(Owl.Maths.combination_float (x + r - 1) x
                            *. (1. -. p) ** of_int r
                            *. p ** of_int x) in
  let mean () = p *. r_float /. (1. -. p) in
  let var () = p *. r_float /. (1. -. p) ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* multivariate distributions are not supported yet *)
(* https://en.wikipedia.org/wiki/Multinomial_distribution *)
(* let multinomial ~n ~p = *)
(*   let k = Array.length p in *)
(*   let n_float = Float.of_int n in *)
(*   ignore ((Float.abs (Owl.Arr.(of_array p [| k |] |> sum |> to_array).(0) -. 1.)) < 0.000001); *)
(*   let sample () = Owl_stats.multinomial_rvs n ~p in *)
(*   let logpdf x = Owl_stats.multinomial_logpdf x ~p in *)
(*   let mean () = Array.map (fun p -> n_float *. p) p in *)
(*   let var () = Array.init k (fun i -> Array.init k (fun j -> *)
(*       if i = j then n_float *. p.(i) *. (1. -. p.(i)) *)
(*       else -. n_float *. p.(i) *. p.(j))) in *)
(*   make ~sample ~logpdf ~mean ~var () *)

(* https://en.wikipedia.org/wiki/Poisson_distribution *)
let poisson ~lambda =
  assert (lambda > 0.);
  (* https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables *)
  let sample () =
    let step = 500. in
    let rec aux lam k p =
      let k = k + 1 in
      let p = ref (p *. Owl_stats.uniform_rvs ~a:0. ~b:1.) in
      let lam = ref lam in
      while !p < 1. && !lam > 0. do
        if !lam > step then (
          p := !p *. exp step ;
          lam := !lam -. step
        ) else (
          p := !p *. exp !lam ;
          lam := 0.
        )
      done ;
      if !p > 1. then aux !lam k !p else k - 1 in
    aux lambda 0 1. in
  let logpdf x = Float.of_int x *. log lambda *. exp (-. lambda) /. Owl.Maths.fact x in
  let mean () = lambda in
  let var () = lambda in
  make ~sample ~logpdf ~mean ~var ()

(* 2. Continuous probability distributions *)

(* https://en.wikipedia.org/wiki/Beta_distribution *)
let beta ~a ~b =
  assert (a > 0. && b > 0.);
  let sample () = Owl_stats.beta_rvs ~a ~b in
  let logpdf x = Owl_stats.beta_logpdf x ~a ~b in
  let mean () = a /. (a +. b) in
  let var () = a *. b /. (((a +. b) ** 2.) *. (a +. b +. 1.)) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Cauchy_distribution *)
let cauchy ~loc ~scale =
  assert (scale > 0.);
  let sample () = Owl_stats.cauchy_rvs ~loc ~scale in
  let logpdf x = Owl_stats.cauchy_logpdf x ~loc ~scale in
  let mean () = nan in
  let var () = nan in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Chi2_distribution *)
let chi2 ~df =
  assert (df > 0.);
  let sample () = Owl_stats.chi2_rvs ~df in
  let logpdf x = Owl_stats.chi2_logpdf x ~df in
  let mean () = df in
  let var () = 2. *. df in
  make ~sample ~logpdf ~mean ~var ()

(* multivariate distributions are not supported yet *)
(* (\* https://en.wikipedia.org/wiki/Dirichlet_distribution *\) *)
(* let dirichlet ~alpha = *)
(*   let alpha_arr = Owl.Arr.of_array alpha [| Array.length alpha |] in *)
(*   let alpha_sum = Owl.Arr.sum alpha_arr in *)
(*   let alpha_normalized = Owl.Arr.(alpha_arr / alpha_sum) in *)
(*   assert (Array.for_all (fun a -> a > 0.) alpha); *)
(*   let sample () = Owl_stats.dirichlet_rvs ~alpha in *)
(*   let logpdf x = Owl_stats.dirichlet_logpdf x ~alpha in *)
(*   let mean () = (Owl.Arr.to_array alpha_normalized).(0) in *)
(*   let var () = Owl.Arr.( *)
(*       ( (diag alpha_normalized) - (mul alpha_normalized (transpose alpha_normalized)) ) *)
(*       / (alpha_sum + Owl.Arr.ones [| 1 |] ) *)
(*       |> to_array *)
(*     ).(0) in *)
(*   make ~sample ~logpdf ~mean ~var () *)

(* https://en.wikipedia.org/wiki/Exponential_distribution *)
let exponential ~lambda =
  assert (lambda > 0.);
  let sample () = Owl_stats.exponential_rvs ~lambda in
  let logpdf x = Owl_stats.exponential_logpdf x ~lambda in
  let mean () = 1. /. lambda in
  let var () = 1. /. (lambda ** 2.) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Generalized_normal_distribution *)
let exponpow ~mu ~a ~b =
  assert (a > 0. && b > 0.);
  let sample () = Owl_stats.exponpow_rvs ~a ~b +. mu in
  let logpdf x = Owl_stats.exponpow_logpdf (x -. mu) ~a ~b in
  let mean () = mu in
  let var () = a ** 2. *. (Owl.Maths.gamma (b /. 3.)) /. (Owl.Maths.gamma (1. /. b)) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/F-distribution *)
let f ~dfnum ~dfden =
  assert (dfnum > 0. && dfden > 0.);
  let sample () = Owl_stats.f_rvs ~dfnum ~dfden in
  let logpdf x = Owl_stats.f_logpdf x ~dfnum ~dfden in
  let mean () = if dfden > 2. then
      dfden /. (dfden -. 2.)
    else nan in
  let var () = if dfden > 4. then
      (2. *. dfden ** 2. *. (dfnum +. dfden -. 2.)) /. (dfnum *. (dfden -. 2.) ** 2. *. (dfden -. 4.))
    else nan in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Gamma_distribution *)
let gamma ~shape ~scale =
  assert (shape > 0. && scale > 0.);
  let sample () = Owl_stats.gamma_rvs ~shape ~scale in
  let logpdf x = Owl_stats.gamma_logpdf x ~shape ~scale in
  let mean () = shape *. scale in
  let var () = shape *. scale ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Normal_distribution *)
let gaussian ~mu ~sigma =
  assert (sigma > 0.);
  let sample () = Owl_stats.gaussian_rvs ~mu ~sigma in
  let logpdf x = Owl_stats.gaussian_logpdf x ~mu ~sigma in
  let mean () = mu in
  let var () = sigma ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Gumbel_distribution *)
let gumbel ~a ~b =
  let eulermascheroni = 0.577215664901532860606512090082402431042 in
  assert (b > 0.);
  let sample () = Owl_stats.gumbel1_rvs ~a ~b in
  let logpdf x = Owl_stats.gumbel1_logpdf x ~a ~b in
  let mean () = a +. b *. eulermascheroni in
  let var () = Float.pi ** 2. *. b ** 2. /. 6. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Laplace_distribution *)
let laplace ~loc ~scale =
  assert (scale > 0.);
  let sample () = Owl_stats.laplace_rvs ~loc ~scale in
  let logpdf x = Owl_stats.laplace_logpdf x ~loc ~scale in
  let mean () = loc in
  let var () = 2. *. scale ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Logistic_distribution *)
let logistic ~loc ~scale =
  assert (scale > 0.);
  let sample () = Owl_stats.logistic_rvs ~loc ~scale in
  let logpdf x = Owl_stats.laplace_logpdf x ~loc ~scale in
  let mean () = loc in
  let var () = scale ** 2. *. Float.pi ** 2. /. 3. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Log-normal_distribution *)
let lognormal ~mu ~sigma =
  assert (sigma > 0.);
  let sample () = Owl_stats.lognormal_rvs ~mu ~sigma in
  let logpdf x = Owl_stats.lognormal_logpdf x ~mu ~sigma in
  let mean () = exp (mu +. sigma ** 2. /. 2.) in
  let var () = (exp (sigma ** 2.) -. 1.) *. exp (2. *. mu +. sigma ** 2.) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Lomax_distribution *)
let lomax ~shape ~scale =
  assert (shape > 0. && scale > 0.);
  let sample () = Owl_stats.lomax_rvs ~shape ~scale in
  let logpdf x = Owl_stats.lomax_logpdf x ~shape ~scale in
  let mean () = if shape > 1. then
      scale /. (shape -. 1.)
    else nan in
  let var () = if shape > 2. then
      scale ** 2. *. shape /. ((shape -. 1.) ** 2. *. (shape -. 2.))
    else (if shape >= 1. then infinity else nan) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Pareto_distribution *)
let pareto ~shape ~scale =
  assert (shape > 0. && scale > 0.);
  (*  https://en.wikipedia.org/wiki/Pareto_distribution#Random_sample_generation *)
  let sample () = scale /. (Owl_stats.uniform_rvs ~a:0. ~b:0. ** (1. /. shape)) in
  let logpdf x = shape *. scale ** shape /. (x ** (shape +. 1.)) in
  let mean () = if shape > 1. then
      shape *. scale /. (shape -. 1.)
    else infinity in
  let var () = if shape > 2. then
      scale ** 2. *. shape /. ((shape -. 1.) ** 2. *. (shape -. 1.))
    else infinity in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Rayleigh_distribution *)
let rayleigh ~sigma =
  assert (sigma > 0.);
  let sample () = Owl_stats.rayleigh_rvs ~sigma in
  let logpdf x = Owl_stats.rayleigh_logpdf x ~sigma in
  let mean () = sigma *. sqrt (Float.pi /. 2.) in
  let var () = (4. -. Float.pi) *. sigma ** 2. /. 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Rice_distribution *)
let rice ~nu ~sigma =
  let laguerre x = exp (x /. 2.) *. ((1. -. x) *. Owl.Maths.i0 (-. x /. 2.) -. x *. Owl.Maths.i1 (-. x /. 2.)) in
  assert (nu > 0. && sigma > 0.);
  (* https://en.wikipedia.org/wiki/Rice_distribution#Related_distributions *)
  let sample () =
    let x, y = Owl_stats.(gaussian_rvs ~mu:nu ~sigma, gaussian_rvs ~mu:0. ~sigma) in
    sqrt (x ** 2. +. y ** 2.) in
  let logpdf x = log x -. 2. *. log sigma -. (x ** 2. +. nu ** 2. /. (2. *. sigma ** 2.)) +. log Owl.Maths.(i0 (x *. nu /. sigma ** 2.)) in
  let mean () = sigma *. sqrt Float.(pi /. 2.) *. laguerre (-. nu ** 2. /. (2. *. sigma ** 2.)) in
  let var () = 2. *. sigma ** 2. +. nu ** 2. -. Float.pi *. sigma ** 2. /. 2. *. (laguerre (-. nu ** 2. /. (2. *. sigma ** 2.))) ** 2. in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Student%27s_t-distribution *)
let t ~df ~loc ~scale =
  assert (df > 0. && scale > 0.);
  let sample () = Owl_stats.t_rvs ~df ~loc ~scale in
  let logpdf x = Owl_stats.t_logpdf x ~df ~loc ~scale in
  let mean () = if df > 1. then loc else nan in
  let var () = if df > 2. then
      scale ** 2. *. df /. (df -. 2.)
    else (if df > 1. then infinity else nan) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Continuous_uniform_distribution *)
let uniform ~a ~b =
  let sample () = Owl_stats.uniform_rvs ~a ~b in
  let logpdf x = Owl_stats.uniform_logpdf x ~a ~b in
  let mean () = a +. (b /. 2.) in
  let var () = 1. /. 12. *. ((b -. a) ** 2.) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Von_Mises_distribution *)
let vonmises ~mu ~kappa =
  assert (kappa > 0.);
  let sample () = Owl_stats.vonmises_rvs ~mu ~kappa in
  let logpdf x = Owl_stats.vonmises_logpdf x ~mu ~kappa in
  let mean () = mu in
  let var () = 1. -. Owl.Maths.(i0 kappa -. i1 kappa) in
  make ~sample ~logpdf ~mean ~var ()

(* https://en.wikipedia.org/wiki/Weibull_distribution *)
let weibull ~shape ~scale =
  assert (shape > 0. && scale > 0.);
  let sample () = Owl_stats.weibull_rvs ~shape ~scale in
  let logpdf x = Owl_stats.weibull_logpdf x ~shape ~scale in
  let mean () = scale *. Owl.Maths.gamma (1. +. 1. /. shape) in
  let var () = scale ** 2. *. Owl.Maths.(gamma (1. +. 2. /. shape) -. gamma (1. +. 1. /. shape) ** 2.) in
  make ~sample ~logpdf ~mean ~var ()
