module type S = sig
  type prob
  val sample : prob -> 'a Distribution.t -> 'a
  val factor : prob -> float -> unit
  val observe : prob -> 'a Distribution.t -> 'a -> unit
  val assume : prob -> bool -> unit
  val infer : (prob -> 'a -> 'b) -> 'a -> 'b Distribution.t
end
module type Parametered = sig
  include S
  type param
  val infer : ?param:param -> (prob -> 'a -> 'b) -> 'a -> 'b Distribution.t
end

module RejectionSampling = struct
  type prob = Prob
  type param = { samples : int }

  let sample _prob d = Distribution.draw d

  exception Reject

  let assume _prob p = if not p then raise Reject

  let factor prob s = assume prob (s <> -.infinity)

  let observe prob d x =
    let y = Distribution.draw d in
    assume prob (x = y)

  let infer ?(param = { samples = 1000 }) model data =
    let rec exec i = try model Prob data with Reject -> exec i in
    let values = Array.init param.samples exec in
    Distribution.uniform_support ~values
end
module RejectionSamplingWith (P : sig val samples : int end) = struct
  include RejectionSampling
  let infer model data = infer ~param:P.{ samples } model data
end

module ImportanceSampling  = struct
  type prob = { id : int; scores : float array }
  type param = { samples : int }

  let sample _prob d = Distribution.draw d
  let factor prob s = prob.scores.(prob.id) <- prob.scores.(prob.id) +. s
  let observe prob d x = factor prob (Distribution.logpdf d x)
  let assume prob p = factor prob (if p then 0. else -.infinity)

  let infer ?(param = { samples = 1000 }) model data =
    let scores = Array.make param.samples 0. in
    let values = Array.mapi (fun i _ -> model { id = i; scores } data) scores in
    Distribution.support ~values ~logits:scores
end
module ImportanceSamplingWith (P : sig val samples : int end) = struct
  include ImportanceSampling
  let infer model data = infer ~param:P.{ samples } model data
end
