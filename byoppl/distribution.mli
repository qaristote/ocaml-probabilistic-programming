(** Distributions and basic statistical functions. Whenever possible samplers and log-density functions use the {{: https://ocaml.xyz/owl/owl/Owl_stats/index.html}Owl_stats} implementation. *)

type 'a t
(** Distribution type. *)

type 'a support = {
  values : 'a array;
  logits : float array;
  probs : float array;
}

exception EmptySupport

(** Weighted support for discrete distributions (probabilities are stored in log scale for numerical stability). *)

val draw : 'a t -> 'a
(** Draw a sample from the distribution. *)

val logpdf : 'a t -> 'a -> float
(** [logpdf dist x] returns the value of the log-density of [dist] on [x]. *)

val get_samples : 'a t -> 'a array
(** Draw 10 000 samples from the distribution. Every call to [get_samples] returns the same array. *)

val get_support : ?shrink:bool -> 'a t -> 'a support
(** Returns the support of a discrete distribution. Raise [Invalid_argument] if support is not defined. If [shrink] is true we gather similar values to shrink the support. *)

(** {1 Basic statistical functions} *)

val mean : float t -> float
(** [mean dist] returns the mean of [dist]. *)

val mean_int : int t -> float
(** Same as [mean] for distributions over integers. *)

val var : float t -> float
(** [var dist] returns the variance of [dist]. *)

val var_int : int t -> float
(** Same as [var] for distributions over integers. *)

val std : float t -> float
(** [std dist] returns the standard deviation of [dist]. *)

val std_int : int t -> float
(** Same as [std] for distributions over integers. *)

val stats : float t -> float * float
(** [stats dist] returns the mean and standard deviation of [dist]. *)

val stats_int : int t -> float * float
(** Same as [stats] for distributions over integers. *)

val map : ?shrink:bool -> ?finv:('b -> 'a) -> ('a -> 'b) -> 'a t -> 'b t
(** [map ~shrink ~finv f dist] pushes the distribution [dist] forward along [f], and
    shrink the support if [shrink] is true. If [finv] is [Some f], [f] is considered
    to be the inverse of [f] and is used to compute the log-likelihood from the
    log-likelihood of [dist].*)

val split : ('a * 'b) t -> 'a t * 'b t
(** [split dist] turns a distribution over pairs into a pair of distributions. *)

val split_list : 'a list t -> 'a t list
(** [split_list dist] turns a distribution over lists into a list of distributions. *)

val split_array : 'a array t -> 'a t array
(** [split_array dist] turns a distribution over arrays into an array of distributions. *)

(** {1 Discrete distributions} *)

val bernoulli : p:float -> int t
(** {{: https://en.wikipedia.org/wiki/Bernoulli_distribution}Bernoulli
    distribution} with parameter [p]. *)

val betabinomial : n:int -> a:float -> b:float -> int t
(** {{: https://en.wikipedia.org/wiki/Beta-binomial_distribution}Beta-binomial
    distribution} with parameters [n], [a] and [b]. *)

val binomial : p:float -> n:int -> int t
(** {{: https://en.wikipedia.org/wiki/Binomial_distribution}Binomial
    distribution} with parameters [p] and [n]. *)

val dirac : v:'a -> 'a t
(** {{: https://en.wikipedia.org/wiki/Dirac_distribution}Dirac
    distribution} on the value [v]. *)

val geometric : p:float -> int t
(** {{: https://en.wikipedia.org/wiki/Geometric_distribution}Geometric
    distribution} with parameter [p]. *)

val hypergeometric : good:int -> bad:int -> sample:int -> int t
(** {{: https://en.wikipedia.org/wiki/Hypergeometric_distribution}Hypergeometric
    distribution} with parameters [good], [bad] and [sample]. *)

val negbinomial : r:int -> p:float -> int t
(** {{: https://en.wikipedia.org/wiki/Negative_binomial_distribution}Negative
    binomial distribution} with parameters [r] and [p]. *)

val poisson : lambda:float -> int t
(** {{: https://en.wikipedia.org/wiki/Poisson_distribution}Poisson distribution}
    with parameter [lambda]. *)

val support : values:'a array -> logits:float array -> 'a t
(** Distribution of a random variable which can take any value [values.(i)] with
    probability [exp logits.(i)] (probabilities are stored in log scale for numerical stability). *)

val uniform_support : values:'a array -> 'a t
(** Support distribution where all values are equiprobable. *)

(** {1 Continuous distributions} *)

val beta : a:float -> b:float -> float t
(** {{: https://en.wikipedia.org/wiki/Beta_distribution}Beta distribution} with
    parameters [a] and [b]. *)

val cauchy : loc:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Cauchy_distribution}Cauchy distribution} with
    parameters [loc] and [scale]. *)

val chi2 : df:float -> float t
(** {{: https://en.wikipedia.org/wiki/Chi2_distribution}Chi2 distribution} with
    parameter [df]. *)

val exponential : lambda:float -> float t
(** {{: https://en.wikipedia.org/wiki/Exponential_distribution}Exponential
    distribution} with parameter [lambda]. *)

val exponpow : mu:float -> a:float -> b:float -> float t
(** {{: https://en.wikipedia.org/wiki/Generalized_normal_distribution}Generalized
    normal distribution} with parameters [mu], [a] and [b]. *)

val f : dfnum:float -> dfden:float -> float t
(** {{: https://en.wikipedia.org/wiki/F-distribution}F distribution} with
    parameters [dfnum] and [dfden]. *)

val gamma : shape:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Gamma_distribution}Gamma distribution}
    with parameters [shape] and [scale]. *)

val gaussian : mu:float -> sigma:float -> float t
(** {{: https://en.wikipedia.org/wiki/Normal_distribution}Gaussian distribution}
    with mean [mu] and standard deviation [sigma]. *)

val gumbel : a:float -> b:float -> float t
(** {{: https://en.wikipedia.org/wiki/Gumbel_distribution}Gumbel distribution}
    with parameters [a] and [b]. *)

val laplace : loc:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Laplace_distribution}Laplace distribution}
    with parameters [loc] and [scale]. *)

val logistic : loc:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Logistic_distribution}Logistic
    distribution} with parameters [loc] and [scale]. *)

val lognormal : mu:float -> sigma:float -> float t
(** {{: https://en.wikipedia.org/wiki/Log-normal_distribution}Log-normal
    distribution} with parameters [mu] and [sigma]. *)

val lomax : shape:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Lomax_distribution}Lomax distribution}
    with parameters [shape] and [scale]. *)

val pareto : shape:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Pareto_distribution}Pareto distribution}
    with parameters [shape] and [scale]. *)

val rayleigh : sigma:float -> float t
(** {{: https://en.wikipedia.org/wiki/Rayleigh_distribution}Rayleigh
    distribution} with parameter [sigma]. *)

val rice : nu:float -> sigma:float -> float t
(** {{: https://en.wikipedia.org/wiki/Rice_distribution}Rice distribution} with
    parameters [nu] and [sigma]. *)

val t : df:float -> loc:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Student%27s_t-distribution}Student's
    t-distribution} with parameters [df], [loc] and [scale]. *)

val uniform : a:float -> b:float -> float t
(** {{: https://en.wikipedia.org/wiki/Continuous_uniform_distribution}Uniform
    distribution} on the segment [[a, b]]. *)

val vonmises : mu:float -> kappa:float -> float t
(** {{: https://en.wikipedia.org/wiki/Von_Mises_distribution}Von Mises
    distribution} with parameters [mu] and [kappa]. *)

val weibull : shape:float -> scale:float -> float t
(** {{: https://en.wikipedia.org/wiki/Weibull_distribution}Weibull distribution}
    with parameters [shape] and [scale]. *)
