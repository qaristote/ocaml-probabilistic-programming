module type S = sig
  type prob
  val sample : prob -> 'a Distribution.t -> 'a
  val factor : prob -> float -> unit
  val observe : prob -> 'a Distribution.t -> 'a -> unit
  val assume : prob -> bool -> unit
  val infer : (prob -> 'a -> 'b) -> 'a -> 'b Distribution.t
end
module type Parametered = sig
  include S
  type param
  val infer : ?param:param ->
    (prob -> 'a -> 'b) -> 'a -> 'b Distribution.t
end

module RejectionSampling : Parametered
module ImportanceSampling : Parametered
module RejectionSamplingWith : functor (P : sig val samples : int end) -> S
  with type prob = RejectionSampling.prob
module ImportanceSamplingWith : functor (P : sig val samples : int end) -> S
  with type prob = ImportanceSampling.prob
