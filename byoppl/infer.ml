module type S = sig
  type 'a prob
  type 'a cont
  val sample : 'a Distribution.t -> ('a -> 'b cont) -> 'b cont
  val factor : float -> (unit -> 'a cont) -> 'a cont
  val observe : 'a Distribution.t -> 'a -> (unit -> 'b cont) -> 'b cont
  val assume : bool -> (unit -> 'a cont) -> 'a cont
  val infer : ('a -> ('b -> 'b cont) -> 'b cont) -> 'a -> 'b Distribution.t
end
module type Parametered = sig
  include S
  type param
  val infer : ?param:param -> ('a -> ('b -> 'b cont) -> 'b cont) -> 'a -> 'b Distribution.t
end

module Basic (B : Basic.S) = struct
  type 'a prob = B.prob
  type 'a cont = 'a prob -> 'a

  let sample d k prob =
    let v = B.sample prob d in
    k v prob

  let factor s k prob =
    let () = B.factor prob s in
    k () prob

  let observe d x k prob =
    let () = B.observe prob d x in
    k () prob

  let assume p k prob =
    let () = B.assume prob p in
    k () prob

  let exit v _ = v

  let infer model =
    B.infer (fun prob data -> (model data) exit prob)
end

module Parametered (I : S) = struct
  include I
  type param = unit
  let infer ?(param = ()) = ignore (param) ; infer
end

module Gen = struct
  type 'a prob = 'a option
  and 'a cont = 'a prob -> 'a prob

  let sample d k prob =
    let v = Distribution.draw d in
    k v prob

  let factor _s k prob = k () prob
  let observe d x = factor (Distribution.logpdf d x)
  let assume p = factor (if p then 0. else -.infinity)
  let exit v _prob = Some v

  let draw model data =
    let v = (model data) exit None in
    Option.get v

  let infer model data =
    Distribution.dirac ~v:(draw model data)
end

module ImportanceSampling = struct
  type 'a prob = { id : int; particles : 'a particle array }
  and 'a particle = { value : 'a option; score : float; k : 'a cont }
  and 'a cont = 'a prob -> 'a prob
  type param = { samples : int }

  let sample d k prob =
    let v = Distribution.draw d in
    k v prob

  let factor s k prob =
    let particle = prob.particles.(prob.id) in
    prob.particles.(prob.id) <- { particle with score = s +. particle.score };
    k () prob

  let assume p = factor (if p then 0. else -.infinity)
  let observe d x = factor (Distribution.logpdf d x)

  let run_cont prob =
    if prob.id < Array.length prob.particles - 1 then
      let k = prob.particles.(prob.id + 1).k in
      k { prob with id = prob.id + 1 }
    else prob

  let exit v prob =
    let particle = prob.particles.(prob.id) in
    prob.particles.(prob.id) <- { particle with value = Some v };
    run_cont prob

  let infer ?(param = { samples = 1000 }) model data =
    let init_particle = { value = None; score = 0.; k = (model data) exit } in
    let prob = { id = -1; particles = Array.make param.samples init_particle } in
    let prob = run_cont prob in
    let values = Array.map (fun p -> Option.get p.value) prob.particles in
    let logits = Array.map (fun p -> p.score) prob.particles in
    Distribution.support ~values ~logits
end
module ImportanceSamplingWith (P : sig val samples : int end) = struct
  include ImportanceSampling
  let infer model data = infer ~param:P.{ samples } model data
end

module ParticleFilter = struct
  include ImportanceSampling

  let resample particles =
    let logits = Array.map (fun x -> x.score) particles in
    let values = Array.map (fun x -> { x with score = 0. }) particles in
    let dist = Distribution.support ~values ~logits in
    Array.init (Array.length particles) (fun _ -> Distribution.draw dist)

  let factor s k prob =
    let particle = prob.particles.(prob.id) in
    prob.particles.(prob.id) <-
      { particle with k = k (); score = s +. particle.score };
    let prob =
      if prob.id < Array.length prob.particles - 1 then prob
      else { id = -1; particles = resample prob.particles }
    in
    run_cont prob

  let assume p = factor (if p then 0. else -.infinity)
  let observe d x = factor (Distribution.logpdf d x)
end
module ParticleFilterWith (P : sig val samples : int end) = struct
  include ParticleFilter
  let infer model data = infer ~param:P.{ samples } model data
end

module Enumeration = struct
  type 'a prob = { score : float ; outputs : ('a * float) list ref }
  type 'a cont = 'a prob -> unit

  let factor s k prob =
    if s = -.infinity then ()
    else k () { prob with score = prob.score +. s }

  let sample d k prob =
    let Distribution.{ values ; logits ; _ } =
      try Distribution.get_support d
      with Invalid_argument _ -> failwith "Enumeration : cannot enumerate a continuous distribution" in
    Array.iter2 (fun v s -> factor s (fun () -> k v) prob) values logits

  let assume p = factor (if p then 0. else -.infinity)
  let observe d x = factor (Distribution.logpdf d x)

  let exit v prob =
    prob.outputs := (v, prob.score) :: !( prob.outputs )

  let infer model data =
    let prob = { score = 0. ; outputs = ref [] } in
    (model data) exit prob;
    let values, logits = !( prob.outputs ) |> List.split in
    let values = values |> Array.of_list in
    let logits = logits |> Array.of_list in
    Distribution.support ~values ~logits
end

module MetropolisHasting = struct
  type 'a prob = { value : 'a option ;
                   length : int ;
                   score : float ;
                   trace : ('a prob) list ;
                   k : 'a cont }
  and 'a cont = 'a prob -> 'a prob
  type param = { warmups : int ; samples : int }

  let flip p = Distribution.(draw (bernoulli ~p)) = 1

  let sample d k prob =
    let v = Distribution.draw d in
    let length = prob.length + 1 in
    let trace = prob :: prob.trace in
    let k = k v in
    let prob = { prob with length ; trace ; k } in
    k prob

  let factor s k prob =
    let score = prob.score +. s in
    let k = k () in
    let prob = { prob with score ; k } in
    k prob

  let observe d x = factor (Distribution.logpdf d x)
  let assume p = factor (if p then 0. else -.infinity)

  let exit v prob =
    { prob with value = Some v }

  let run_prob prob = prob.k prob

  let infer ?(param = { warmups = 100 ; samples = 1000 }) model data =
    let prob = ref { value = None ;
                     length = 0 ;
                     score = 0. ;
                     trace = [] ;
                     k = (model data) exit } in
    prob := run_prob !prob ;
    if !prob.length = 0 then failwith "Metropolis-Hasting : nothing to resample" ;

    let resample () =
      let prob_old = !prob in
      let sample = List.nth prob_old.trace (Random.int prob_old.length) in
      let prob_new = run_prob sample in
      let p = if prob_new.score = -.infinity then 0.
        else min 1. (Float.(of_int prob_old.length /. of_int prob_new.length)
                     *. exp (prob_new.score -. prob_old.score)) in
      prob := if flip p then
          prob_new
        else prob_old ;
      !prob in

    for _ = 0 to param.warmups do
      ignore (resample ()) ;
    done ;
    let logits = Array.make param.samples 0. in
    let values = Array.init param.samples (fun i ->
        let prob = resample () in
        logits.(i) <- prob.score ;
        Option.get prob.value ) in
    Distribution.support ~values ~logits
end

module MetropolisHastingWith (P : sig val warmups : int val samples : int end) = struct
  include MetropolisHasting
  let infer model data = infer ~param:P.{ warmups ; samples } model data
end
