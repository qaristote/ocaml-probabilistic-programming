module type S = sig
  type 'a prob
  type 'a cont
  val sample : 'a Distribution.t -> ('a -> 'b cont) -> 'b cont
  val factor : float -> (unit -> 'a cont) -> 'a cont
  val observe : 'a Distribution.t -> 'a -> (unit -> 'b cont) -> 'b cont
  val assume : bool -> (unit -> 'a cont) -> 'a cont
  val infer : ('a -> ('b -> 'b cont) -> 'b cont) -> 'a -> 'b Distribution.t
end
module type Parametered = sig
  include S
  type param
  val infer : ?param:param ->
    ('a -> ('b -> 'b cont) -> 'b cont) -> 'a -> 'b Distribution.t
end

module Basic : functor (B : Basic.S) -> S
module Parametered : functor (I : S) -> Parametered

module Gen : S
module ImportanceSampling : Parametered
module ImportanceSamplingWith : functor (P : sig val samples : int end) -> S
  with type 'a prob = 'a ImportanceSampling.prob
   and type 'a cont = 'a ImportanceSampling.cont
module ParticleFilter : Parametered
  with type 'a prob = 'a ImportanceSampling.prob
   and type 'a cont = 'a ImportanceSampling.cont
module ParticleFilterWith : functor (P : sig val samples : int end) -> S
  with type 'a prob = 'a ParticleFilter.prob
   and type 'a cont = 'a ParticleFilter.cont
module Enumeration : S
module MetropolisHasting : Parametered
module MetropolisHastingWith : functor (P : sig val warmups : int val samples : int end) -> S
  with type 'a prob = 'a MetropolisHasting.prob
   and type 'a cont = 'a MetropolisHasting.cont
