open Byoppl

module TrueSkills (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  type tournament = { n : int ; duels : (int * int) list }

  let model { n ; duels } =
    let skills = Array.make n 0. in
    let* () = Cps_array.iteri (fun i _ ->
        let* skill = I.sample (Distribution.gaussian ~mu:100. ~sigma:10.) in
        skills.(i) <- skill ;
        return ())
        skills in
    let* () = Cps_list.iter (fun (i,j) ->
        let skillW = skills.(i) in
        let skillL = skills.(j) in
        let* perfW = I.sample (Distribution.gaussian ~mu:skillW ~sigma:13.) in
        let* perfL = I.sample (Distribution.gaussian ~mu:skillL ~sigma:13.) in
        let* () = I.assume (perfW > perfL) in
        return ())
        duels in
    return skills 

  let duels n =
    let flip () = Distribution.(draw (bernoulli ~p:0.5)) = 1 in
    List.init (n * (n - 1)) (fun i -> let player1 = i / (n - 1) in
                              let player2 = i mod (n - 1) in
                              let player2 = player2 + if player2 >= player1 then 1 else 0 in
                              if flip () then (player1, player2) else (player2, player1))

  let print_stats n =
    Format.printf "@.-- TrueSkills, %s --@." Name.name ;
    let data = { n ;
                 duels = duels n } in
    List.iter (fun (i,j) -> Format.printf "%d wins against %d@." i j) data.duels ;
    let dists = I.infer model data in
    let dists = dists |> Distribution.split_array in
    let stats = Array.map Distribution.stats dists in
    Array.iteri (fun i (mean, std) -> Format.printf "Player %d : %f (+- %f)@." i mean std) stats
end

module P = struct let samples = 1900 end

module RS = Infer.Basic (Basic.RejectionSamplingWith (P))
module TS_RS = TrueSkills (struct let name = "Basic Rejection Sampling" end) (RS)
let _ = TS_RS.print_stats 3

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module TS_IS_B = TrueSkills (struct let name = "Basic Importance Sampling" end) (IS)
let _ = TS_IS_B.print_stats 3

module TS_IS_CPS = TrueSkills (struct let name = "CPS Importance Sampling" end)
    (Infer.ImportanceSamplingWith (P))
let _ = TS_IS_CPS.print_stats 3

module TS_PF = TrueSkills (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = TS_PF.print_stats 3

module TS_MH = TrueSkills (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = TS_MH.print_stats 3
