open Byoppl

module HiddenMarkovModel (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model data =
    let rec gen states data =
      match (states, data) with
      | [], y :: data -> gen [ y ] data
      | states, [] -> return states
      | pre_x :: _, y :: data ->
        let* x = I.sample (Distribution.gaussian ~mu:pre_x ~sigma:1.0) in
        let* () = I.observe (Distribution.gaussian ~mu:x ~sigma:1.0) y in
        gen (x :: states) data
    in
    gen [] data

  let print_approx () =
    Format.printf "@.-- HMM, %s --@." Name.name ;
    let data =
      Owl.Arr.(linspace 0. 20. 20 + gaussian [| 20 |])
      |> Owl.Arr.to_array |> Array.to_list
    in
    let dist = Distribution.split_list (I.infer model data) in
    let stats = List.map Distribution.stats (List.rev dist) in
    List.iter2 (fun point (mean, std) -> Format.printf "%f -> %f (+- %f)@." point mean std) data stats
end

module P = struct let samples = 1000 end

(* module RS = Infer.Basic (Basic.RejectionSamplingWith (N)) *)
(* module HMM_RS = HiddenMarkovModel (struct let name = "Basic Rejection Sampling" end) (RS) *)
(* let _ = HMM_RS.print_approx () *)

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module HMM_IS_B = HiddenMarkovModel (struct let name = "Basic Importance Sampling" end) (IS)
let _ = HMM_IS_B.print_approx ()

module HMM_IS_CPS = HiddenMarkovModel (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = HMM_IS_CPS.print_approx ()

module HMM_PF = HiddenMarkovModel (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = HMM_PF.print_approx ()

module HMM_MH = HiddenMarkovModel (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = HMM_MH.print_approx ()
