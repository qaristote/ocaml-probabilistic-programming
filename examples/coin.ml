open Byoppl

module Coin (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model data =
    let* z = I.sample (Distribution.uniform ~a:0. ~b:1.) in
    let* () = Cps_list.iter (I.observe (Distribution.bernoulli ~p:z)) data in
    return z

  let print_stats () =
    Format.printf "@.-- Coin, %s --@." Name.name ;
    let dist = I.infer model [ 0 ; 1 ; 1 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ] in
    let m, s = Distribution.stats dist in
    Format.printf "Coin bias, mean : %f std : %f @." m s
end

module P = struct let samples = 1000 end

module RS = Infer.Basic (Basic.RejectionSamplingWith (P))
module C_RS = Coin (struct let name = "Basic Rejection Sampling" end) (RS)
let _ = C_RS.print_stats ()

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module C_IS_B = Coin (struct let name = "Basic Importance Sampling" end) (IS)
let _ = C_IS_B.print_stats ()

module C_IS_CPS = Coin (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = C_IS_CPS.print_stats ()

module C_PF = Coin (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = C_PF.print_stats ()

module C_MH = Coin (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = C_MH.print_stats ()
