open Byoppl

module FunnyBernoulli (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model () =
    let* a = I.sample (Distribution.bernoulli ~p:0.5) in
    let* b = I.sample (Distribution.bernoulli ~p:0.5) in
    let* c = I.sample (Distribution.bernoulli ~p:0.5) in
    let* () = I.assume (a = 1 || b = 1) in
    return (a + b + c)


  let print_support () =
    Format.printf "@.-- Funny Bernoulli, %s --@." Name.name ;
    let dist = I.infer model () in
    let Distribution.{ values ; probs ; _ } = Distribution.get_support ~shrink:true dist in
    Array.iteri (fun i x -> Format.printf "%d %f@." x probs.(i)) values

  let print_samples n =
    Format.printf "@.-- Funny Bernoulli, %s --@." Name.name ;
    for _ = 1 to n do
      let dist = I.infer model () in
      let v = Distribution.draw dist in
      Format.printf "%d " v
    done ;
    Format.printf "@."
end

module P = struct let samples = 1000 end

module RS = Infer.Basic (Basic.RejectionSamplingWith (P))
module FB_RS = FunnyBernoulli (struct let name = "Basic Rejection Sampling" end) (RS)
let _ = FB_RS.print_support ()

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module FB_IS_B = FunnyBernoulli (struct let name = "Basic Importance Sampling" end) (IS)
let _ = FB_IS_B.print_support ()

module FB_G = FunnyBernoulli (struct let name = "CPS Generation" end) (Infer.Gen)
let _ = FB_G.print_samples 10

module FB_IS_CPS = FunnyBernoulli (struct let name = "CPS Importance Sampling" end)
    (Infer.ImportanceSamplingWith (P))
let _ = FB_IS_CPS.print_support ()

module FB_PF = FunnyBernoulli (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = FB_PF.print_support ()

module FB_E = FunnyBernoulli (struct let name = "CPS Enumeration" end) (Infer.Enumeration)
let _ = FB_E.print_support ()

module FB_MH = FunnyBernoulli (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = FB_MH.print_support ()
