open Byoppl

module Platforms (Name : sig val name : string end) (I : Infer.S) = struct
  type position = { x : float ; y : float }
  type speed = { vx : float ; vy : float }
  (* a platform of width [width] centered at [pos] along the line of
     equation [y = slope * (x - pos.x)  + pos.y]*)
  type platform = { slope : float ; pos : position ; radius : float }
  (* the gravitional acceleration on earth *)
  let g = -. 9.81 (* m/s² *)

  let distance { x = x1 ; y = y1 } { x = x2 ; y = y2 } =
    sqrt ((x1 -. x2) ** 2. +. (y1 -. y2) ** 2.)

  let roots a b c =
    let delta = b ** 2. -. 4. *. a *. c in
    let x0 = -. b /. (2. *. a) in
    if delta < 0. then []
    else (if delta = 0. then [ x0 ]
          else let width = sqrt delta /. (2. *. a) in
            [ x0 -. width ; x0 +. width ])

  let intersect_platform pos speed platform =
    (* x = pos.x + t * speed.vx *)
    (* y = platform.slope * (x - platform.pos.x) + platform.pos.y *)
    (* y = pos.y + t * speed.vy + t² * g/2 *)
    (* hence (aX² + bX + c)(t) = 0 with *)
    let a = g /. 2. in
    let b = speed.vy -. speed.vx *. platform.slope in
    let c = pos.y -. platform.slope *. (pos.x -. platform.pos.x) -. platform.pos.y in
    let pos_next t = { x = pos.x +. t *. speed.vx ;
                       y = t ** 2. *. g /. 2. +. t *. speed.vy +. pos.y } in
    let speed_next t = { speed with vy = speed.vy +. t *. g } in
    roots a b c
    (* remove solutions corresponding to time traveling backwards or standing still *)
    |> List.filter (fun t -> t > 0.0000000001)
    (* decorate the solution with additional information *)
    |> List.map (fun t -> (t, pos_next t, speed_next t, platform))
    (* remove solutions corresponding to intersection points not on the actual platform *)
    |> List.filter (fun (_, p, _, _) -> distance p platform.pos < platform.radius)
    (* remove solutions corresponding to intersection points below the floor *)
    |> List.filter (fun (_, p, _, _) -> p.y >= 0.)

  let rebound_on slope speed =
    let parallel = (speed.vx +. speed.vy *. slope) /. (1. +. slope ** 2.) in
    { vx = 2. *. parallel -. speed.vx  ;
      vy = 2. *. parallel *. slope -. speed.vy }

  let simulate ~timeout ~platforms ?(speed = { vx = 0. ; vy = 0. }) pos =
    assert (pos.y > 0.) ;
    let floor = { slope = 0. ;
                  pos = { x = 0. ; y = 0.00001 } ;
                  radius = infinity } in
    let rec step t0 pos speed =
      let next = intersect_platform pos speed in
      let next_floor = match next floor with
        | [ next ] -> next
        | l ->
          Format.printf "%d solutions : " (List.length l) ; failwith "infinite energy" in
      let nexts = platforms
                  |> List.map (intersect_platform pos speed)
                  |> List.flatten in
      let t, pos, speed, platform =
        List.fold_left (fun ((t1, _, _, _) as next1) ((t2, _, _, _) as next2) ->
            if t1 < t2 then next1 else next2)
          next_floor nexts in
      let t = t0 +. t in
      if platform = floor then
        Some pos.x
      else
        if t < timeout then
          let speed_new = rebound_on platform.slope speed in
          step t pos speed_new
        else
          None in
    step 0. pos speed

  let timeout_counter = ref 0
  let distance_to_hole_from ~hole_pos ~timeout ~platforms ?(speed = { vx = 0. ; vy = 0. }) pos =
    match simulate ~timeout ~platforms ~speed pos with
    | None -> incr timeout_counter ; infinity
    | Some x -> abs_float (x -. hole_pos)

  open Cps_operators
  type conditions = { pos : position ;
                      platform_radii : float list ;
                      hole_pos : float ;
                      hole_radius : float ;
                      timeout : float }
  let model { pos ; platform_radii ; hole_pos ; hole_radius ; timeout } =
    let* platforms = Cps_list.mapi (fun i radius ->
        let* slope = if i = 0 then (
            let* negslope = I.sample Distribution.(exponential ~lambda:1.) in
            return (-. negslope))
          else I.sample Distribution.(gaussian ~mu:0. ~sigma:1.) in
        let* pos_platform =
          let* x = if i = 0 then return 0. else I.sample Distribution.(gaussian ~mu:0. ~sigma:2.) in
          let* y = I.sample Distribution.(uniform ~a:0. ~b:pos.y) in
          return { x ; y } in
        return { slope ; pos = pos_platform ; radius }) platform_radii in
    let* () = I.observe
        Distribution.(gaussian ~mu:0. ~sigma:(hole_radius /. 5.))
        (distance_to_hole_from ~hole_pos ~timeout ~platforms pos) in
    return platforms

  let print_stats () =
    Format.printf "@.-- Platforms, %s --@." Name.name ;
    let conditions = { pos =  { x = 0. ; y = 5. } ;
                       platform_radii = [ 1. ; 1. ] ;
                       hole_pos = 13. ;
                       (* Distribution.(draw (exponential ~lambda:0.1)) ; *)
                       hole_radius = 1. ;
                       (* 4 * #platforms * time to fall from height to 0 *)
                       timeout = 4. *. 2. *. sqrt (-. 2. *. 5. /. g)
                     } in
    Format.printf "Dropping a ball from %f meters high.@." conditions.pos.y ;
    Format.printf "Aiming for the hole %f meters on the right.@." conditions.hole_pos ;
    try
      let dist = I.infer model conditions in
      let dist_platforms = Distribution.split_list dist in
      List.iteri (fun i dist_platform ->
          Format.printf "Platform %d :@." (i + 1) ;
          let stats = dist_platform
                      |> Distribution.map (fun platform -> [| platform.slope ; platform.pos.x ; platform.pos.y |])
                      |> Distribution.split_array
                      |> Array.map Distribution.stats in
          Array.iter2
            (fun stats name -> Format.printf "%s = %f (+- %f)@." name (fst stats) (snd stats))
            stats [| "slope" ; "x" ; "y" |];
        ) dist_platforms ;
      let dist_result = Distribution.map
          (fun platforms -> distance_to_hole_from
              ~hole_pos:conditions.hole_pos
              ~timeout:conditions.timeout
              ~platforms
              conditions.pos)
          dist in
      Format.printf "On average the ball falls %f meters from the center of the hole.@." Distribution.(mean dist_result)
    with | Distribution.EmptySupport -> Format.printf "No solution found. Timedout %d times.@." !timeout_counter
end

module P = struct let samples = 10000 end

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module P_IS_B = Platforms (struct let name = "Basic Importance Sampling" end) (IS)
let _ = P_IS_B.print_stats ()

module P_IS_CPS = Platforms (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = P_IS_CPS.print_stats ()

module P_PF = Platforms (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = P_PF.print_stats ()

module P_MH = Platforms (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 5000 let samples = 5000 end))
let _ = P_MH.print_stats ()
