open Byoppl

module Laplace (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model () =
    let* p = I.sample (Distribution.uniform ~a:0. ~b:1.) in
    let* () = I.observe (Distribution.binomial ~p ~n:493_472) 241_945 in
    return p

  let print_stats () =
    Format.printf "@.-- Laplace, %s --@." Name.name ;
    let dist = I.infer model () in
    let m, s = Distribution.stats dist in
    Format.printf "Gender bias, mean : %f std : %f @." m s
end

module P = struct let samples = 1000 end

(* module RS = Infer.Basic (Basic.RejectionSamplingWith (N)) *)
(* module L_RS = Laplace (struct let name = "Basic Rejection Sampling" end) (RS) *)
(* let _ = L_RS.print_stats () *)

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module L_IS_B = Laplace (struct let name = "Basic Importance Sampling" end) (IS)
let _ = L_IS_B.print_stats ()

module L_IS_CPS = Laplace (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = L_IS_CPS.print_stats ()

module L_PF = Laplace (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = L_PF.print_stats ()

module L_MH = Laplace (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = L_MH.print_stats ()
