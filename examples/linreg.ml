open Byoppl

module LinearRegression (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators

  let model (xs, ys) =
    let* a = I.sample (Distribution.gaussian ~mu:0. ~sigma:1.) in
    let* b = I.sample (Distribution.gaussian ~mu:0. ~sigma:1.) in
    let* sigma = I.sample (Distribution.gaussian ~mu:0. ~sigma:1.) in
    let* () = Cps_array.iter2 (fun x y ->
        let mu = a *. x +. b in
        let sigma = abs_float sigma in
        I.observe (Distribution.gaussian ~mu ~sigma) y)
      xs ys in
    return [| a ; b ; sigma |]

  let print_stats n =
    Format.printf "@.-- Linear Regression, %s --@." Name.name ;
    let a_true = Distribution.(draw (gaussian ~mu:0. ~sigma:1.)) in
    let b_true = Distribution.(draw (gaussian ~mu:0. ~sigma:1.)) in
    let sigma_true = abs_float (Distribution.(draw (gaussian ~mu:0. ~sigma:1.))) in
    Format.printf "y ~ N(%f * x + %f, %f)@." a_true b_true sigma_true ;
    let xs = Array.init n (fun _ -> Distribution.(draw (gaussian ~mu:0. ~sigma:1.))) in
    let ys = Array.map (fun x ->
        let mu = a_true *. x +. b_true in
        let sigma = sigma_true in
        Distribution.(draw (gaussian ~mu ~sigma)))
        xs in
    let dists = I.infer model (xs, ys) |> Distribution.split_array in
    let stats = Array.map Distribution.stats dists in
    Array.iter2 (fun name (mean, std) -> Format.printf "%s = %f (+- %f)@." name mean std) [| "a" ; "b" ; "sigma" |] stats
end

let n = 10
module P = struct let samples = 1000 end

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module LR_IS_B = LinearRegression (struct let name = "Basic Importance Sampling" end) (IS)
let _ = LR_IS_B.print_stats n

module LR_IS_CPS = LinearRegression (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = LR_IS_CPS.print_stats n

module LR_PF = LinearRegression (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = LR_PF.print_stats n

module LR_MH = LinearRegression (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = LR_MH.print_stats n
