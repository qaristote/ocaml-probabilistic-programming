open Byoppl

module KnuthYao (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model () =
    let rec chain n =
      let* coin = I.sample (Distribution.bernoulli ~p:0.5) in
      match n with
      | 0 -> if coin = 1 then chain 1 else chain 2
      | 1 -> if coin = 1 then chain 3 else chain 4
      | 2 -> if coin = 1 then chain 5 else chain 6
      | 3 -> if coin = 1 then chain 1 else return 1
      | 4 -> if coin = 1 then return 2 else return 3
      | 5 -> if coin = 1 then return 4 else return 5
      | 6 -> if coin = 1 then chain 2 else return 6
      | _ -> raise (Invalid_argument (Format.sprintf "%d" n)) in
    chain 0

  let print_support () =
    Format.printf "@.-- Knuth-Yao, %s --@." Name.name ;
    let dist = I.infer model () in
    let Distribution.{ values ; probs ; _ } = Distribution.get_support ~shrink:true dist in
    Array.iteri (fun i x -> Format.printf "%d %f@." x probs.(i)) values

  let print_samples n =
    Format.printf "@.-- Knuth-Yao, %s --@." Name.name ;
    for _ = 1 to n do
      let dist = I.infer model () in
      let v = Distribution.draw dist in
      Format.printf "%d " v
    done ;
    Format.printf "@."
end

module P = struct let samples = 1000 end

module RS = Infer.Basic (Basic.RejectionSamplingWith (P))
module KY_RS = KnuthYao (struct let name = "Basic Rejection Sampling" end) (RS)
let _ = KY_RS.print_support ()

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module KY_IS_B = KnuthYao (struct let name = "Basic Importance Sampling" end) (IS)
let _ = KY_IS_B.print_support ()

module KY_G = KnuthYao (struct let name = "CPS Generation" end) (Infer.Gen)
let _ = KY_G.print_samples 10

module KY_IS_CPS = KnuthYao (struct let name = "CPS Importance Sampling" end)
    (Infer.ImportanceSamplingWith (P))
let _ = KY_IS_CPS.print_support ()

module KY_PF = KnuthYao (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = KY_PF.print_support ()

(* Stack overflow (arbitrarily long traces) *)
(* module KY_E = KnuthYao (struct let name = "CPS Enumeration" end) (Infer.Enumeration) *)
(* let _ = KY_E.print_support () *)

module KY_MH = KnuthYao (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 1000 let samples = 1000 end))
let _ = KY_MH.print_support ()
