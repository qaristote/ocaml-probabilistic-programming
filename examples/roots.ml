open Byoppl

module Roots (Name : sig val name : string end) (I : Infer.S) = struct
  open Cps_operators
  let model (f, error) =
    let* x = I.sample (Distribution.gaussian ~mu:0. ~sigma:1.) in
    let* () = I.assume (abs_float (f x) < error) in
    return x

  let print_stats n =
    Format.printf "@.-- Root finding, %s --@." Name.name ;
    let root = Distribution.(draw (gaussian ~mu:0. ~sigma:1.)) in
    let coeffs = Array.init n (fun _ -> Distribution.(draw (exponential ~lambda:1.))) in
    let f x = (x -. root) *. (let x_sq = x ** 2. in Array.fold_left (fun tmp coeff -> coeff +. x_sq *. tmp) 0. coeffs) in
    Format.printf "f(x) = (x - %f) * P(x**2)@." root ;
    try
      let dist = I.infer model (f, 0.001) in
      let mean, std = Distribution.stats dist in
      Format.printf "x0 = %f +- %f@." mean std ;
      let fdist = Distribution.map f dist in
      let fmean, fstd = Distribution.stats fdist in
      Format.printf "f(x0) ~= %f +- %f@." fmean fstd
    with | Distribution.EmptySupport -> Format.printf "No solution found.@."
end

let n = 10
module P = struct let samples = 10000 end

module IS = Infer.Basic (Basic.ImportanceSamplingWith (P))
module R_IS_B = Roots (struct let name = "Basic Importance Sampling" end) (IS)
let _ = R_IS_B.print_stats n

module R_IS_CPS = Roots (struct let name = "CPS Importance Sampling" end) (Infer.ImportanceSamplingWith (P))
let _ = R_IS_CPS.print_stats n

module R_PF = Roots (struct let name = "CPS Particle Filter" end) (Infer.ParticleFilterWith (P))
let _ = R_PF.print_stats n

module R_MH = Roots (struct let name = "CPS Metropolis-Hasting" end)
    (Infer.MetropolisHastingWith (struct let warmups = 5000 let samples = 5000 end))
let _ = R_MH.print_stats n
